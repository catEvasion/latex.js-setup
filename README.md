# Latex.js Setup

## Installation

Start with installing Volta: https://docs.volta.sh/guide/getting-started

It takes care of the correct node version.

Prepare all dependencies by running:

        npm i 

## Usage

Use the following command to convert all `.tex` documents located in the `documets` folder and it's sub folders.

        npm run latex

### Custom Macros

Edit the `customMacros.js` file to implement custom macros. 

### Custom CSS

Extend the `custom.css` file for new/additional styles.