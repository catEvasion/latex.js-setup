import { HtmlGenerator } from 'latex.js';
import { createHTMLWindow } from 'svgdom';

global.window = createHTMLWindow();
global.document = window.document;

const folderPath = 'node_modules/latex.js/dist/css';

export const generator = new HtmlGenerator({
  CustomMacros: (function () {
    var args = CustomMacros.args = {},
      prototype = CustomMacros.prototype;

    function CustomMacros(generator) {
      this.g = generator;
    }

    args['bf'] = ['HV']
    prototype['bf'] = function () {
      this.g.setFontWeight('bf')
    };

    return CustomMacros;
  }()),
  styles: ['css/custom.css']
});