import { parse } from 'latex.js';
import fs from 'fs';
import path from 'path';
import { generator } from './customMacros.js'

const folderPath = path.resolve(path.dirname(new URL(import.meta.url).pathname), "documents");
const suffix = '.tex';
readFilesWithSuffix(folderPath, suffix, (err, filePaths) => {
    if (err) {
        console.error('An error occured :', err);
        return;
    }
    console.log('Files to convert:');
    filePaths.forEach((filePath) => {
        console.log(filePath);
        convertFile(filePath);
    });
});

function convertFile(inputFile) {
    fs.readFile(inputFile, 'utf8', (error, latex) => {
        if (error) {
            console.error('An error occurred while reading the file:', error);
            return;
        }
        let doc = parse(latex, { generator: generator }).htmlDocument();

        const html = doc.documentElement.outerHTML;
        generator.reset();

        const outputFile = inputFile.slice(0, suffix.length * -1) + '.html';

        fs.writeFile(outputFile, html, (error) => {
            if (error) {
                console.error('An error occurred:', error);
                return;
            }
            console.log('Created: '+ outputFile);
        });
    });
};

function readFilesWithSuffix(folderPath, suffix, callback) {
    fs.readdir(folderPath, (err, files) => {
        if (err) {
            callback(err, null);
            return;
        }

        const filePaths = [];
        const promises = [];

        files.forEach((file) => {
            const filePath = path.join(folderPath, file);
            promises.push(new Promise((resolve, reject) => {
                fs.stat(filePath, (err, stats) => {
                    if (err) {
                        reject(err);
                        return;
                    }
                    if (stats.isDirectory()) {
                        readFilesWithSuffix(filePath, suffix, (err, subFiles) => {
                            if (err) {
                                reject(err);
                                return;
                            }
                            filePaths.push(...subFiles);
                            resolve();
                        });
                    } else if (stats.isFile() && file.endsWith(suffix)) {
                        filePaths.push(filePath);
                        resolve();
                    } else {
                        resolve();
                    }
                });
            }));
        });

        Promise.all(promises)
            .then(() => {
                callback(null, filePaths);
            })
            .catch((err) => {
                callback(err, null);
            });
    });
}
